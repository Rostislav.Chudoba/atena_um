// mainpage User Material for Atena Documentation
// _pageUserMat User Material for Atena Documentation
/** \mainpage Implementing User Material in Atena - Reference and Example Documentation
 *  Copyright Cervenka Consulting 2008-2014
 *
 * \subpage UserMaterialIntro "User Material Introduction"
 *
 * \ref CCUserMaterialExampleDLL.c "CCUserMaterialExampleDLL.c File Reference"
 *
 * \ref CCUserMaterialFORTRANExampleDLL.F90 "CCUserMaterialFORTRANExampleDLL.F90 File Reference"
 */
 /** \page UserMaterialIntro User Material Introduction
  *  Copyright Cervenka Consulting 2008-2014
  *
  * Written by Dobromil Pryl
  *
  * \section userdll_intro_sec Introduction
  *
  * This is a description for users who wish to implement their own
  * material model into ATENA version 5 when the material behavior
  * can not be described by the standard materials or by user laws
  * in the fracture-plastic material (CC3DNonLinCementitious2User).
  * It is assumed the user already has experience using ATENA,
  * including basic understanding of the input file (.inp),
  * some background knowledge about the model to be implemented
  * and material models and finite element method (FEM) in general,
  * and that he or she is able to develop in C or some
  * other programming language that can produce dynamic link libraries,
  * e.g., FORTRAN or Pascal.
  *
  * Basically, the user has to prepare a dynamic link library (DLL) which exports
  * several functions that will be called by the ATENA kernel.
  * Probably the easiest way is to start with a copy of the example project
  * included with ATENA (CCUserMaterialExampleDLL), which implements
  * an incremental elastic material with von Mises stress as an additional state
  * variable for available for postprocessing.
  *
  * \section userdll_operation Operation
  *
  * Before starting the implementation, one should know how the new
  * user material library will be used by the kernel,
  * or, in other words, understand when and what for which of the functions
  * to be implemented are needed.
  *
  * \subsection userdll_definition Material Definition
  *
  * Let us first have a look what happens when ATENA reads an input file where
  * <tt>CC3DUserMaterial</tt> is defined, e.g.
  * <br>
  * <tt> <br>
  * MATERIAL ID 1 NAME "3D User" <br>
  * TYPE "CC3DUserMaterial" <br>
  *    UserMaterialDLL "CCUserMaterialExampleDLL.dll" <br>
  *    E       3.032000e+004 <br>
  *    ALPHA   1.200000e-005 <br>
  *    RHO     2.300000e-002 <br>
  *    MU      2.000000e-001 <br>
  *    YieldStress       20.0 <br>
  * ; <br>
  * </tt>
  * <br>
  * The first line just defines a new material and its ID (internal number)
  * and name.
  * The second line is a bit more interesting and tells the kernel what
  * kind of material to create. In this case, the type <tt>CC3DUserMaterial</tt>
  * means that it is going to be a user defined material.
  * When the <br>
  * <tt>UserMaterialDLL "CCUserMaterialExampleDLL.dll"</tt> <br>
  * line is read,
  * the user library is loaded (or an error reported if it can not be found or loaded).
  * Then, all the interface functions are loaded (or, again, an error is reported
  * if some of them is
  * missing or does not conform with respect to the expected parameters and return type).
  * Afterwards, a few of the user functions are called to get several values needed
  * to create the material in memory: <br>
  * - the number of user material parameters are asked for by calling the function
  * \ref UserDLLMaterialParamsCount() <br>
  * - the number of user state variables from a call to \ref UserDLLStateVarsCount() <br>
  * - all the user material parameter names are requested from \ref UserDLLMaterialParamName()
  * and stored such that they can be recognized when processing the following lines of the input. <br>
  *
  * Then, the remaining material parameter values are read and stored.
  * Please understand that the <tt>UserMaterialDLL</tt> parameter has to be the first one simply because
  * the others, except for those inherited from the ATENA elastic material (i.e.,
  * <tt>E</tt>, <tt>mu</tt>, <tt>ALPHA</tt>, and <tt>RHO</tt>)
  * can not be known to the kernel at all before the user DLL is loaded.
  *
  * \note Please note the difference between <em>user material parameters</em>
  * and <em>user state variables</em>.
  * While the former are properties of the <em>material</em> and do not change during the analysis,
  * the latter are defined separately in each <em>material point</em> and their main purpose is to
  * reflect the (local) changes in the material.
  * For both of them, only floating point values are allowed in the current implementation.
  *
  * \subsection userdll_init Material Point Initialization
  *
  * Before the analysis starts, quantities that should be available
  * for postprocessing have to be registered. Stresses and strains are inherited
  * from the elastic material, and therefore available automatically.
  * The names of the additional user state variables are
  * obtained by a call to \ref UserDLLStateVarName().
  * Furthemore, all the integration points of all finite elements
  * are initialized (e.g., all strains and stresses to zero). In case of the user material,
  * \ref UserDLLResetState() is called for each material point such that the user state variables
  * can be initialized to any initial values as needed.
  *
  * \subsection userdll_calc Calculation
  *
  * In nonlinear analysis, the applied load is divided into load steps
  * and in each step, a nonlinear system of equations is solved by (linear) iterations
  * (see the description of the <em>Newton-Raphson</em> and <em>Arclength</em> methods
  * in <em>Atena Theory Manual</em> for details).
  * For each iteration, a linear approximation system is built
  * (when using the <em>Full Newton-Raphson</em> method, there are small differences
  * in other cases) and solved.
  * The global stiffness matrix of the linear system is built from local tangent stiffness matrices.
  * In case of user material, \ref UserDLLTangentStiff() is called for each material point to
  * get the local matrices.
  *
  * The material response to a deformation increment is asked
  * (possibly multiple times) in each iteration for each material point.
  * For the user material, this has to be implemented in \ref UserDLLCalculateResponse(),
  * which is obviously the most interesting and most important
  * function of the user material library.
  *
  * During the nonlinear analysis, the coordinate system changes due to large deformations
  * have to be taken into account. If anything else than the default transformation
  * of strains and stresses based on the deformation gradients is needed,
  * i.e., additional variables have to be transformed, or the stresses or strains have to be
  * transformed in some different way, this can be done in \ref UserDLLTransformState().
  *
  * \section userdll_implementation Implementation
  *
  * All the above mentioned functions are implemented and commented in \ref CCUserMaterialExampleDLL.c
  * (C and all languages except FORTRAN)
  * and \ref CCUserMaterialFORTRANExampleDLL.F90 "CCUserMaterialFORTRANExampleDLL.F90"
  * (FORTRAN). There, you can find the description of all functions, arguments, and return values.
  * Please note FORTRAN needs special handling due to specific argument
  * and return-value passing. Therefore, if you create your
  * User Material Dynamic Link Library in FORTRAN, you have to include
  * the string "FORTRAN" in the DLL file name (and, obviously, the
  * file names of DLLs created in other languages must NOT contain
  * this string).
  *
  * This documentation is based on the example DLL included in ATENA installation.
  * Therefore, the global arrays
  * with the names of material parameters and state variables and the corresponding #defines used
  * for their dimensions and for some auxiliary values, are also included.
  * These are not compulsory, they are just a suggestion how the user can implement
  * the functions that return the parameters' and variables' count and names
  * and handle some floating point calculation aspects in an easy and consistent way.
  *
  * See also the example input file <tt>UserMaterial.inp</tt> demonstrating the use
  * of a user defined material
  * (included in ATENA installation in the
  * <tt>"Examples\ATENA Science\AtenaWin\CCUserMaterialExampleDLL"</tt>
  * subdirectory of the ATENA installation directory, typically,
  * <tt>"c:\Program Files\CervenkaConsulting\AtenaV5"</tt>,
  * and the ATENA Theory and ATENA Input File Format manuals.
  *
  */
  /**
   *  @file CCUserMaterialExampleDLL.c
   *
   *  \brief Purpose: File containing an example user material
   *                  definition - elastic material with von Mises
   *                  effective stress made available for postprocessing.
   *                  For users implementing their own user materials
   *                  it is recommended to use this file or the whole
   *                  project as a base for their own development,
   *                  i.e., to copy and modify this.
   *
   *                  The project is created in Microsoft Visual Studio 2012,
   *                  however, it should be a good guideline for implementing
   *                  an ATENA user material in any C/C++ compiler.
   *                  Moreover, any language/compiler which can produce a DLL
   *                  implementing the interface functions, e.g., FORTRAN or Pascal,
   *                  can be used. Please note FORTRAN needs special handling and
   *                  see \ref CCUserMaterialFORTRANExampleDLL
   *                  for a FORTRAN example and functions' description.
   *                  For an overview of operation, see the \ref UserMaterialIntro
   *                  "User Material Introduction".
   *
   * The sources of the example DLL and sample ATENA input files (.inp)
   * are included in ATENA installation. You can find them in the
   * <tt>"Examples\ATENA Science\AtenaWin\CCUserMaterialExampleDLL"</tt>
   * subdirectory of the ATENA installation directory (typically,
   * <tt>"c:\Program Files\CervenkaConsulting\AtenaV5"</tt>).
   *
   *  Author: Dobromil Pryl
   *
   *  Revision history:
   *
   *              1. 2008   - the file was created
   *
   * \sa class CCUserMaterial (ATENA internal class wrapping the user material DLL)
   *
   */

#include <windows.h>
#include <float.h>
#include <math.h>
#include <excpt.h>
#include <stdio.h>


   //#include "CCUserMaterialDLL.h"

   /** @name Global variables of UserMaterialDLL
	*
	* @{
	*/

	/** Number of user material parameters = floating point
	 *  values read from the user material definition in the input file.
	 *  This value is returned by \ref UserDLLMaterialParamsCount()
	 *
	 *  Not a compulsory part of the interface - the user may implement
	 *  the name returning function in another way.
	 */
#define UserMaterialParamsCount 3
	 /** All user material parameter names in an array, which is used
	  *  in \ref UserDLLMaterialParamName()
	  *
	  *  Not a compulsory part of the interface - the user may implement
	  *  the name returning function in another way.
	  */
static char* UserMaterialParamNames[UserMaterialParamsCount] =
{
   "e_p",
   "e_f",
   "c_T"
};

/** Number of user state variables = floating point
 *  values stored in each material point.
 *  This value is also returned by \ref UserDLLStateVarsCount()
 *
 *  Not a compulsory part of the interface - the user may implement
 *  the name returning function in another way.
 */
#define N_DIM 3
#define N_ENG 6
#define N_MICROPLANES 28

 // microplane normals
static double MPN[N_MICROPLANES][N_DIM] =
{ {.577350259, .577350259, .577350259},
  {.577350259, .577350259, -.577350259},
  {.577350259, -.577350259, .577350259},
  {.577350259, -.577350259, -.577350259},
  {.935113132, .250562787, .250562787},
  {.935113132, .250562787, -.250562787},
  {.935113132, -.250562787, .250562787},
  {.935113132, -.250562787, -.250562787},
  {.250562787, .935113132, .250562787},
  {.250562787, .935113132, -.250562787},
  {.250562787, -.935113132, .250562787},
  {.250562787, -.935113132, -.250562787},
  {.250562787, .250562787, .935113132},
  {.250562787, .250562787, -.935113132},
  {.250562787, -.250562787, .935113132},
  {.250562787, -.250562787, -.935113132},
  {.186156720, .694746614, .694746614},
  {.186156720, .694746614, -.694746614},
  {.186156720, -.694746614, .694746614},
  {.186156720, -.694746614, -.694746614},
  {.694746614, .186156720, .694746614},
  {.694746614, .186156720, -.694746614},
  {.694746614, -.186156720, .694746614},
  {.694746614, -.186156720, -.694746614},
  {.694746614, .694746614, .186156720},
  {.694746614, .694746614, -.186156720},
  {.694746614, -.694746614, .186156720},
  {.694746614, -.694746614, -.186156720} };

// weight of each microplane
static double MPW[N_MICROPLANES] =
{ .0160714276 * 6.0, .0160714276 * 6.0, .0160714276 * 6.0, .0160714276 * 6.0, .0204744730 * 6.0,
	.0204744730 * 6.0, .0204744730 * 6.0, .0204744730 * 6.0, .0204744730 * 6.0, .0204744730 * 6.0,
	.0204744730 * 6.0, .0204744730 * 6.0, .0204744730 * 6.0, .0204744730 * 6.0, .0204744730 * 6.0,
	.0204744730 * 6.0, .0158350505 * 6.0, .0158350505 * 6.0, .0158350505 * 6.0, .0158350505 * 6.0,
	.0158350505 * 6.0, .0158350505 * 6.0, .0158350505 * 6.0, .0158350505 * 6.0, .0158350505 * 6.0,
	.0158350505 * 6.0, .0158350505 * 6.0, .0158350505 * 6.0 };

#define ZERO(arr,N_I) for (int i = 0; i < N_I; i++) arr[i] = 0;
#define IJ(N_I,I,J) (N_I * J + I)
#define IJK(N_I,N_J,I,J,K) (N_I * IJ(N_J,J,K) + I)
#define IJKL(N_I,N_J,N_K,I,J,K,L) (N_I * IJK(N_J,N_K,J,K,L) + I)

int MPNN_nij_initialized = 0;
double MPNN_nij[N_MICROPLANES * N_DIM * N_DIM];
void calculate_MPNN()
{
	// dyadic product of the microplane normals
	// return array([outer(mpn, mpn) for mpn in self._MPN]) # old
	// implementation
	// n identify the microplane
	//MPNN_nij = einsum('ni,nj->nij', self._MPN, self._MPN)

	ZERO(MPNN_nij, N_MICROPLANES * N_DIM * N_DIM)
		for (int m = 0; m < N_MICROPLANES; m++) {
			for (int j = 0; j < N_DIM; j++) {
				for (int i = 0; i < N_DIM; i++) {
					MPNN_nij[IJK(N_DIM, N_DIM, i, j, m)] =
						//MPNN_nij[(m * N_DIM + j) * N_DIM + i] +=   
						MPN[m][j] * MPN[m][i];
				}
			}
		}
	MPNN_nij_initialized = 1;
}

double* get_MPNN() {
	if (MPNN_nij_initialized == 0)
		printf("ERROR: MPNN not initialized\n");
	return MPNN_nij;
}

double e_t[N_DIM * N_DIM];
double* VtoT_transf(double e_v[]) {
	ZERO(e_t, N_DIM * N_DIM)

		for (int j = 0; j < N_DIM; j++) {
			for (int i = 0; i < N_DIM; i++) {
				e_t[IJ(N_DIM, j, i)] =
					0.5 * e_v[(2 * N_DIM) - i - j];
			}
		}

	for (int i = 0; i < N_DIM; i++) {
		e_t[IJ(N_DIM, i, i)] =
			e_v[i];
	}
	/*e_t[0, 1] = 0.5 * e_v[5];
	e_t[1, 0] = 0.5 * e_v[5];
	e_t[0, 2] = 0.5 * e_v[4];
	e_t[2, 0] = 0.5 * e_v[4];
	e_t[1, 2] = 0.5 * e_v[3];
	e_t[2, 1] = 0.5 * e_v[3];*/
	return e_t;
}

double s_v[2 * N_DIM];
double* TtoV_transf(double s_t[]) {
	ZERO(s_v, 2 * N_DIM)

		for (int j = 0; j < N_DIM; j++) {
			for (int i = 0; i < N_DIM; i++) {
				s_v[(2 * N_DIM) - i - j] =
					s_t[IJ(N_DIM, j, i)];
			}
		}

	for (int i = 0; i < N_DIM; i++) {
		s_v[i] =
			s_t[IJ(N_DIM, i, i)];

	}
	return s_v;
}

double e_ni[N_MICROPLANES * N_DIM];
double* get_e_vct_arr(double eps_eng[])
{
	// Projection of apparent strain onto the individual microplanes
	ZERO(e_ni, N_MICROPLANES * N_DIM)
		for (int n = 0; n < N_MICROPLANES; n++) { //was N_DIM
			for (int i = 0; i < N_DIM; i++) {
				for (int j = 0; j < N_DIM; j++) {
					e_ni[IJ(N_DIM, i, n)] +=
						MPN[n][j] * eps_eng[IJ(N_DIM, i, j)];
					//einsum('nj,ji->ni', self._MPN, eps_eng)
				}
			}
		}
	return e_ni;
}

double eN_m[N_MICROPLANES];
double* get_e_N_arr(double e_vct_arr[])
{
	// get the normal strain array for each microplane
	ZERO(eN_m, N_MICROPLANES)
		//eN_n = einsum('ni,ni->n', e_vct_arr, self._MPN)
		for (int m = 0; m < N_MICROPLANES; m++) {
			for (int i = 0; i < N_DIM; i++) {
				eN_m[m] += e_vct_arr[IJ(N_DIM, i, m)] * MPN[m][i];
			}
		}
	return eN_m;
}

double e_T_vct_arr[N_MICROPLANES * N_DIM];
double* get_e_T_vct_arr(double e_vct_arr[])
{
	// get the tangential strain vector array for each microplane
	double* eN_m = get_e_N_arr(e_vct_arr);
	for (int i = 0; i < N_MICROPLANES * N_DIM; i++) {
		e_T_vct_arr[i] = e_vct_arr[i];
	}

	// eN_vct_ni = einsum('n,ni->ni', eN_n, self._MPN)
	for (int m = 0; m < N_MICROPLANES; m++) {
		for (int i = 0; i < N_DIM; i++) {
			e_T_vct_arr[IJ(N_DIM, i, m)] -=
				eN_m[m] * MPN[m][i];
		}
	}
	return e_T_vct_arr;
}

double e_equiv_arr[N_MICROPLANES];
double eN_pos_arr[N_MICROPLANES];
double e_TT_arr[N_MICROPLANES];
double* get_e_equiv_arr(double e_vct_arr[], double c_T)
{
	double* eN_m = get_e_N_arr(e_vct_arr);
	double* e_T_vct_arr = get_e_T_vct_arr(e_vct_arr);
	ZERO(eN_pos_arr, N_MICROPLANES)
		for (int m = 0; m < N_MICROPLANES; m++) {
			eN_pos_arr[m] =
				(fabs(eN_m[m]) + eN_m[m]) / 2;
			for (int i = 0; i < N_DIM; i++) {
				e_TT_arr[m] +=
					e_T_vct_arr[IJ(N_DIM, i, m)] * e_T_vct_arr[IJ(N_DIM, i, m)];
			}
			e_equiv_arr[m] =
				sqrt(eN_pos_arr[m] * eN_pos_arr[m] + c_T * e_TT_arr[m]);
		}
	return e_equiv_arr;
}

/* // not needed since it gives the same as get_e_equiv_arr
double get_state_variables[N_MICROPLANES];
double* get_get_state_variables(double e_vct_arr[], double c_T) {

}
*/



#define UserStateVarsCount 29 //previously N_MICROPLANES
/** All user state variable names in an array, which is used
 *  in \ref UserDLLStateVarName()
 *
 *  Not a compulsory part of the interface - the user may implement
 *  the name returning function in another way.
 */
static char* UserStateVarNames[UserStateVarsCount] =
{
   "phi_0","phi_1","phi_2","phi_3","phi_4","phi_5","phi_6","phi_7","phi_8","phi_9",
   "phi_10","phi_11","phi_12","phi_13","phi_14","phi_15","phi_16","phi_17","phi_18","phi_19",
   "phi_20","phi_21","phi_22","phi_23","phi_24","phi_25","phi_26","phi_27","D_max"
};
/** @} */

/** Largest real number (value from the floating point library)
 *
 *  Not a compulsory part of the interface - the user may implement
 *  in another way.
 */
#define REAL_MAX DBL_MAX
 /** Minimum accepted divisor used to prevent division by zero/overflow
  * (based on a value from the floating point library)
  *
  *  Not a compulsory part of the interface - the user may implement
  *  in another way.
  */
#define MIN_DIV DBL_EPSILON*1000


  // forward declaration (needed in calculate_response)

__declspec(dllexport) UINT CDECL UserDLLTangentStiff(double TangentMatrix[6 * 6],
	double E, double mu, double UserMaterialParams[], double UserMaterialState[]);


double phi_trial[N_MICROPLANES];
//double* phi_ = &phi[0];

double* get_phi_trial(double e_equiv_arr[], double e_p, double e_f)
{
	int i;

	for (i = 0; i < N_MICROPLANES; i++) {
		if (e_equiv_arr[i] >= e_p) {
			phi_trial[i] = sqrt(
				(e_p / e_equiv_arr[i]) * exp(-(e_equiv_arr[i] - e_p) / (e_f - e_p))
			);
		}
		else {
			phi_trial[i] = 1;
		}
	}
	return phi_trial;
}


double phi_arr[N_MICROPLANES];
double* get_phi_arr(double teps[], double e_p, double e_f, double c_T)
{
	// Returns a list of the integrity factors for all microplanes.
	//double* e_T_vct_arr = get_e_T_vct_arr(e_ni);
	double* e_max_arr = get_e_equiv_arr(e_ni, c_T);  //this still has to be defined!!
	//ZERO(phi_arr, N_MICROPLANES)
	for (int i = 0; i < N_MICROPLANES; i++) {
		phi_arr[i] =
			get_phi_trial(e_equiv_arr, e_p, e_f)[i];
	}
	return phi_arr;
}

double phi_ij[N_DIM * N_DIM];
double* get_phi_mtx(double teps[], double phi_arr[], double e_p, double e_f, double c_T) //added a 1 next to phi_arr in order to have the global phi_arr used
{
	// Returns the 2nd order damage tensor 'phi_mtx'

	// scalar integrity factor for each microplane
	ZERO(phi_ij, N_DIM * N_DIM)

		//phi_arr = get_phi_arr(teps, e_p, e_f, c_T);

		// integration terms for each microplanes
		//phi_ij = einsum('n,n,nij->ij', phi_arr, self._MPW, self._MPNN)
		double* MPNN = get_MPNN();

	for (int m = 0; m < N_MICROPLANES; m++) {
		for (int j = 0; j < N_DIM; j++) {
			for (int i = 0; i < N_DIM; i++) {
				//phi_ij[j * N_DIM + i] = 0;
				phi_ij[IJ(N_DIM, i, j)] +=
					//phi_arr[m] * MPW[m] * MPNN[(m* N_DIM + j)* N_DIM + i];
					phi_arr[m] * MPW[m] * MPNN[IJK(N_DIM, N_DIM, i, j, m)];
			}
		}
	}
	return phi_ij;
}

double delta[] = { 1, 0, 0,
				   0, 1, 0,
				   0, 0, 1 };
double beta_ijkl[N_DIM * N_DIM * N_DIM * N_DIM];
double* get_beta_tns(double teps[], double phi_arr[], double e_p, double e_f, double c_T)
{
	ZERO(beta_ijkl, N_DIM * N_DIM * N_DIM * N_DIM)
		for (int i = 0; i < N_DIM; i++) {
			for (int j = 0; j < N_DIM; j++) {
				for (int k = 0; k < N_DIM; k++) {
					for (int l = 0; l < N_DIM; l++) {
						//beta_ijkl[(((i * N_DIM) + j) * N_DIM + k) * N_DIM + l] = 0;
						beta_ijkl[IJKL(N_DIM, N_DIM, N_DIM, i, j, k, l)] +=
							0.25 * (phi_ij[IJ(N_DIM, k, i)] * delta[IJ(N_DIM, l, j)]
								+ phi_ij[IJ(N_DIM, l, i)] * delta[IJ(N_DIM, k, j)]
								+ phi_ij[IJ(N_DIM, k, j)] * delta[IJ(N_DIM, l, i)]
								+ phi_ij[IJ(N_DIM, l, j)] * delta[IJ(N_DIM, k, i)]);

						/*0.25 * (phi_ij[i * N_DIM + k] * delta[j * N_DIM + l]
							+ phi_ij[i * N_DIM + l] * delta[j * N_DIM + k]
							+ phi_ij[j * N_DIM + k] * delta[i * N_DIM + l]
							+ phi_ij[j * N_DIM + l] * delta[i * N_DIM + k]);
					*/
					}
				}
			}
		}
	return beta_ijkl;
}

int D_ijkl_initialized = 0;
double D_ijkl[N_DIM * N_DIM * N_DIM * N_DIM];
void calculate_elasticity_tensor(double E, double nu)
{
	double la = E * nu / ((1.0 + nu) * (1.0 - 2.0 * nu));
	double mu = E / (2.0 + 2.0 * nu);
	ZERO(D_ijkl, N_DIM * N_DIM * N_DIM * N_DIM)

		for (int i = 0; i < N_DIM; i++) {
			for (int j = 0; j < N_DIM; j++) {
				for (int k = 0; k < N_DIM; k++) {
					for (int l = 0; l < N_DIM; l++) {
						D_ijkl[IJKL(N_DIM, N_DIM, N_DIM, i, j, k, l)] +=
							la * delta[IJ(N_DIM, j, i)] * delta[IJ(N_DIM, l, k)]
							+ mu * delta[IJ(N_DIM, k, i)] * delta[IJ(N_DIM, l, j)]
							+ mu * delta[IJ(N_DIM, l, i)] * delta[IJ(N_DIM, k, j)];
						//la * delta[i * N_DIM + j] * delta[k * N_DIM + l]
						//+ mu * delta[i * N_DIM + k] * delta[j * N_DIM + l]
						//+ mu * delta[i * N_DIM + l] * delta[j * N_DIM + k];
					}
				}
			}
		}
	D_ijkl_initialized = 1;
}

double* get_elasticity_tensor()
{
	if (D_ijkl_initialized == 0)
		printf("ERROR: D_ijkl not initialized\n");
	return D_ijkl;

}

/** @name User defined functions of UserMaterialDLL
 * \sa class CCUserMaterial
 * @{
 */
 /**
  *
  * \brief Purpose: calculates the stress response of a material point to given strain.
  *
  *                 This is the key function the user has to define for a new user defined material.
  *                 It is called when evaluating the material response ("material iterations").
  *                 E.g., if a force response is prescribed (in all or 1 direction), this routine
  *                 will be called repeatedly with changing deps until the difference (error)
  *                 is acceptable.
  */

double D4_mdm_ijmn[N_DIM * N_DIM * N_DIM * N_DIM];
double* get_D4_mdm_ijmn(double beta_ijkl[])
{
	ZERO(D4_mdm_ijmn, N_DIM * N_DIM * N_DIM * N_DIM)
		//ZERO(sig_eng, N_DIM * N_DIM)

		for (int i = 0; i < N_DIM; i++) {
			for (int j = 0; j < N_DIM; j++) {
				for (int k = 0; k < N_DIM; k++) {
					for (int l = 0; l < N_DIM; l++) {
						for (int p = 0; p < N_DIM; p++) {
							for (int q = 0; q < N_DIM; q++) {
								for (int r = 0; r < N_DIM; r++) {
									for (int s = 0; s < N_DIM; s++) {
										D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, i, j, k, l)] +=
											D_ijkl[IJKL(N_DIM, N_DIM, N_DIM, p, q, r, s)] * beta_ijkl[IJKL(N_DIM, N_DIM, N_DIM, i, j, p, q)] * beta_ijkl[IJKL(N_DIM, N_DIM, N_DIM, k, l, r, s)];
									}
								}
							}
						}
					}
				}
			}
		}
	return D4_mdm_ijmn;
}

//double D_max_plane[N_MICROPLANES];
double D_max[1];
double* get_max(double arr1[])
{
	ZERO(D_max, 1);
	
	for (int i = 0; i < N_MICROPLANES; i++) {
		//D_max_plane[i] = 0;
		if (arr1[i] > D_max[0]) {
			D_max[0] = arr1[i];
		}
	}
	//for (int i = 0; i < N_MICROPLANES; i++) {
	//	if (arr1[i] == D_max) {
	//		D_max_plane[n] = i;
	//		n = n + 1;
	//	}
	//}
	return D_max;
}

__declspec(dllexport) UINT CDECL UserDLLCalculateResponse(
	double deps[], /**< strain increment tensor stored as a vector,
					*   first the diagonal terms, followed by the off-diagonal
					*/
	double teps[], ///< total strain tensor (stored as a vector)
	double sigma[], ///< [in+out] stress tensor stored as a vector (as for deps)
	double E, ///< elastic modulus
	double mu, ///< Poisson's ratio
	double UserMaterialParams[], ///< vector of user material parameter values
	double UserMaterialState[] ///< [in+out] vector of user material state variables in the material point being calculated 
/**
 * \retval 0 OK
 * \retval nonzero error
 */
) {
	double* e_t_eng = VtoT_transf(teps);

	calculate_MPNN();
	calculate_elasticity_tensor(E, mu);
	double dsigma[6]; // stress increment array
	int i, j;
	double TangentMatrix[6 * 6];
	double s1122, s2233, s1133; // aux. diag. terms
	double s12, s23, s13; // aux. nondiag. terms
	double sigma_t[N_DIM * N_DIM];
	ZERO(sigma_t, N_DIM * N_DIM);
	// get the state and material parameters

	//double* phi_arr = UserMaterialState;
	double e_p = UserMaterialParams[0];
	double e_f = UserMaterialParams[1];
	double c_T = UserMaterialParams[2];

	double* phi_trial = get_phi_trial(e_t_eng, e_p, e_f);
	double* MPNN = get_MPNN();
	double* e_ni = get_e_vct_arr(e_t_eng);
	double* eN_m = get_e_N_arr(e_ni);
	double* e_T_vct_arr = get_e_T_vct_arr(e_ni);
	double* phi_arr = get_phi_arr(e_t_eng, e_p, e_f, c_T);
	double* phi_ij = get_phi_mtx(e_t_eng, phi_arr, e_p, e_f, c_T);
	double* beta_ijkl = get_beta_tns(e_t_eng, phi_arr, e_p, e_f, c_T);
	double* D_ijkl = get_elasticity_tensor(E, mu); // mu here is actually nu, but poisson was wrongly called "mu" in the entire code so i changed it here
	double* D4_mdm_ijmn = get_D4_mdm_ijmn(beta_ijkl);
	
	/*for (int i = 0; i < N_DIM; i++) {
		for (int j = 0; j < N_DIM; j++) {
			for (int m = 0; m < N_DIM; m++) {
				for (int n = 0; n < N_DIM; n++) {
					sigma_t[IJ(N_DIM, j, i)] +=
							D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, i, j, m, n)] * e_t_eng[IJ(N_DIM, n, m)];
				}
			}
		}
	}

sigma = TtoV_transf(sigma_t);

	printf("\n");
	printf("sigma1");
	printf("\n");
	for (int i = 0; i < 6; i++) {
			printf("%.10e ", sigma[i]);
		printf("\n");
	}*/

	UserDLLTangentStiff(TangentMatrix, E, mu, UserMaterialParams, UserMaterialState);

	for (i = 0; i < 6; i++) {
		sigma[i] = 0.;
		for (j = 0; j < 6; j++) {
			sigma[i] += TangentMatrix[i * 6 + j] * teps[j];
		}
	}

	//GitkrakenTest

	printf("\n");
	printf("sigma2");
	printf("\n");
	for (int i = 0; i < 6; i++) {
		printf("%.10e ", sigma[i]);
		printf("\n");
	}

	for (int i = 0; i < N_MICROPLANES; i++) {
		UserMaterialState[i] = phi_arr[i];
	}

	/*
	for (int i = 0; i < N_DIM; i++) {
		UserMaterialState[N_MICROPLANES + i] =
			(1 - fabs(phi_ij[i * (N_DIM+1)])) / (sqrt(((1 - fabs(phi_ij[0])) * (1 - fabs(phi_ij[0])) + (1 - fabs(phi_ij[4])) * (1 - fabs(phi_ij[4])) + (1 - fabs(phi_ij[8])) * (1 - fabs(phi_ij[8])))));
	}*/
	 
	
	//calculation of principal damage direction 

	double damage_arr[N_MICROPLANES];
	ZERO(damage_arr, N_MICROPLANES);
	//double damage_max;
	/*double damage_dir_tensor[N_MICROPLANES * N_DIM];
	ZERO(damage_dir_tensor, N_MICROPLANES * N_DIM);
	double damage_dir_sum[N_DIM];
	ZERO(damage_dir_sum, N_DIM);*/

	
	for (int i = 0; i < N_MICROPLANES; i++) {
		damage_arr[i] = 1 - phi_arr[i];
	}

	
	double* D_max = get_max(damage_arr);
	
	UserMaterialState[N_MICROPLANES] = D_max[0];
	

	/*for (int m = 0; m < N_MICROPLANES; m++) {
		for (int i = 0; i < N_DIM; i++) {
			damage_dir_tensor[IJ(N_DIM, i, m)] =
				damage_arr[m] * MPW[m] * MPN[m][i]; //not sure if MPW should be considered here
		}
	}

	for (int i = 0; i < N_DIM; i++) {
		for (int m = 0; m < N_MICROPLANES; m++) {
			damage_dir_sum[i] +=
				damage_dir_tensor[IJ(N_DIM, i, m)];
		}
	}

	for (int i = 0; i < N_DIM; i++) {
		UserMaterialState[N_MICROPLANES+i] =
			damage_dir_sum[i] / (sqrt(damage_dir_sum[0] * damage_dir_sum[0] + damage_dir_sum[1] * damage_dir_sum[1] + damage_dir_sum[2] * damage_dir_sum[2]));
	}*/
	

	
	/*
	//   __try{
		  // and elastic stress increment
	for (i = 0; i < 6; i++) {
		dsigma[i] = 0.;
		for (j = 0; j < 6; j++) {
			dsigma[i] += TangentMatrix[i * 6 + j] * deps[j];
		}
	}

	// add the increment to the original stress
	for (i = 0; i < 6; i++) {
		sigma[i] += dsigma[i];
	}

	*/

	/*// user state variables
	// vonMisesStress 
	s1122 = sigma[0] - sigma[1];
	s2233 = sigma[1] - sigma[2];
	s1133 = sigma[0] - sigma[2];
	s12 = sigma[3];
	s23 = sigma[4];
	s13 = sigma[5];*/
	// store the von Mises effective stress into the state variable
	/*UserMaterialState[0] =
		sqrt(0.5 * (s1122 * s1122 + s2233 * s2233 + s1133 * s1133)
			+ 3.0 * (s12 * s12 + s13 * s13 + s23 * s23)); // /0.0;
*/
	//   }
	/* handling floating point exceptions here does not work in VS2008
		  - the next fp operation in another module causes a fp-stack exception
	 */
	 //   __except( EXCEPTION_CONTINUE_SEARCH )
	 //   __except( EXCEPTION_EXECUTE_HANDLER )
	 /*   __except( (   (GetExceptionCode()==EXCEPTION_FLT_DENORMAL_OPERAND)
					||(GetExceptionCode()==EXCEPTION_FLT_DIVIDE_BY_ZERO)
					||(GetExceptionCode()==EXCEPTION_FLT_INVALID_OPERATION)
					||(GetExceptionCode()==EXCEPTION_FLT_OVERFLOW)
					||(GetExceptionCode()==EXCEPTION_FLT_STACK_CHECK)
					||(GetExceptionCode()==EXCEPTION_FLT_UNDERFLOW)
				  ) ? EXCEPTION_EXECUTE_HANDLER : EXCEPTION_CONTINUE_SEARCH )
		{  // a floating point exception occured - (almost) infinite von Mises stress
		   _clearfp(); // clear the flags
		   UserMaterialState[0]=50000; //REAL_MAX / 10.0; // divide by a factor to leave some space for graphics output interpolation etc.
		}
	 */

	 /*
	  // Yielding - check the stress criterion
		 UserMaterialState[1] = 0.0;
		 if (UserMaterialState[0] > UserMaterialParams[0]) {
			 UserMaterialState[1] = 1.0;
		 }
	 */
	return 0;
}

/**
 *
 * \brief Purpose: resets (initializes) the material point state.
 */
__declspec(dllexport) UINT CDECL UserDLLResetState(
	double E, ///< Young modulus
	double mu, ///< Poisson's ratio 
	double UserMaterialParams[], ///< user material parameters array
	double UserMaterialState[] ///< [out] user state variables array
 /**
  * \retval 0 OK
  * \retval nonzero error
  */
) {
	int i;
	for (i = 0; i < UserStateVarsCount; i++) {
		UserMaterialState[i] = 0.0;
	}

	calculate_MPNN();
	return 0;
}

/**
 *
 * \brief Purpose: compute the local tangential stiffness matrix.
 */
__declspec(dllexport) UINT CDECL UserDLLTangentStiff(
	double TangentMatrix[6 * 6], ///< [out] the local tangential stiffness matrix as a vector
	double E, ///< Young modulus
	double mu, ///< Poisson's ratio
	double UserMaterialParams[], ///< user material parameters array
	double UserMaterialState[] ///< user state variables array

 /**
  * \retval 0 OK
  * \retval nonzero error
  */
) {
	ZERO(TangentMatrix, 4 * N_DIM * N_DIM);
	/*double const1 = REAL_MAX;
	double const2 = REAL_MAX;
	double const3 = REAL_MAX;
	int i, j;

	// Diagonal terms
	double nomin = 1.0 - mu;
	double denom = (1.0 + mu) * (1.0 - 2.0 * mu);
	if (fabs(denom) > MIN_DIV* fabs(nomin))
	{
		const1 = E * (nomin / denom);
	}
	// Off-diagonal terms
	nomin = mu;
	denom = 1.0 - mu;
	if (fabs(denom) > MIN_DIV* fabs(nomin))
	{
		const2 = const1 * (nomin / denom);
	}
	// Shear terms
	nomin = 1.0 - 2.0 * mu;
	denom = 2.0 * (1.0 - mu);
	if (fabs(denom) > MIN_DIV* fabs(nomin))
	{
		const3 = const1 * (nomin / denom);
	}

	for (i = 0; i < 6; i++) {
		for (j = 0; j < 6; j++) {
			if (i == j) { // diagonal term
				if (i < 3) {
					TangentMatrix[i * 6 + j] = const1;
				}
				else { // Shear terms
					TangentMatrix[i * 6 + j] = const3;
				}
			}
			else { // nondiagonal
				if (i < 3 && j < 3) {
					TangentMatrix[i * 6 + j] = const2;
				}
				else { // Shear terms
					TangentMatrix[i * 6 + j] = 0.;
				}
			}
		}
	}*/
	//instead of previous tangent stiff, the remapping from fourth order tensor to 6 * 6 is done here
	//shorten this				
	TangentMatrix[IJ(2 * N_DIM, 0, 0)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 0, 0, 0, 0)];
	TangentMatrix[IJ(2 * N_DIM, 1, 1)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 1, 1, 1, 1)];
	TangentMatrix[IJ(2 * N_DIM, 2, 2)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 2, 2, 2, 2)];
	TangentMatrix[IJ(2 * N_DIM, 3, 3)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 1, 2, 1, 2)];
	TangentMatrix[IJ(2 * N_DIM, 4, 4)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 0, 2, 0, 2)];
	TangentMatrix[IJ(2 * N_DIM, 5, 5)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 0, 1, 0, 1)];

	TangentMatrix[IJ(2 * N_DIM, 0, 1)] = TangentMatrix[IJ(2 * N_DIM, 1, 0)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 0, 0, 1, 1)];
	TangentMatrix[IJ(2 * N_DIM, 0, 2)] = TangentMatrix[IJ(2 * N_DIM, 2, 0)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 0, 0, 2, 2)];
	TangentMatrix[IJ(2 * N_DIM, 0, 3)] = TangentMatrix[IJ(2 * N_DIM, 3, 0)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 0, 0, 1, 2)];
	TangentMatrix[IJ(2 * N_DIM, 0, 4)] = TangentMatrix[IJ(2 * N_DIM, 4, 0)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 0, 0, 0, 2)];
	TangentMatrix[IJ(2 * N_DIM, 0, 5)] = TangentMatrix[IJ(2 * N_DIM, 5, 0)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 0, 0, 0, 1)];

	TangentMatrix[IJ(2 * N_DIM, 1, 2)] = TangentMatrix[IJ(2 * N_DIM, 2, 1)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 1, 1, 2, 2)];
	TangentMatrix[IJ(2 * N_DIM, 1, 3)] = TangentMatrix[IJ(2 * N_DIM, 3, 1)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 1, 1, 1, 2)];
	TangentMatrix[IJ(2 * N_DIM, 1, 4)] = TangentMatrix[IJ(2 * N_DIM, 4, 1)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 1, 1, 0, 2)];
	TangentMatrix[IJ(2 * N_DIM, 1, 5)] = TangentMatrix[IJ(2 * N_DIM, 5, 1)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 1, 1, 0, 1)];

	TangentMatrix[IJ(2 * N_DIM, 2, 3)] = TangentMatrix[IJ(2 * N_DIM, 3, 2)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 2, 2, 1, 2)];
	TangentMatrix[IJ(2 * N_DIM, 2, 4)] = TangentMatrix[IJ(2 * N_DIM, 4, 2)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 2, 2, 0, 2)];
	TangentMatrix[IJ(2 * N_DIM, 2, 5)] = TangentMatrix[IJ(2 * N_DIM, 5, 2)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 2, 2, 0, 1)];

	TangentMatrix[IJ(2 * N_DIM, 3, 4)] = TangentMatrix[IJ(2 * N_DIM, 4, 3)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 1, 2, 0, 2)];
	TangentMatrix[IJ(2 * N_DIM, 3, 5)] = TangentMatrix[IJ(2 * N_DIM, 5, 3)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 1, 2, 0, 1)];

	TangentMatrix[IJ(2 * N_DIM, 4, 5)] = TangentMatrix[IJ(2 * N_DIM, 5, 4)] = D4_mdm_ijmn[IJKL(N_DIM, N_DIM, N_DIM, 0, 2, 0, 1)];

	return 0;
}

/**
 *
 * \brief Purpose: compute the local elastic stiffness matrix.
 * This is usefull for anizotropic materials.
 * If the function is not defined, the default elastic matrix (based on E and mu) is used.
 */
__declspec(dllexport) UINT CDECL UserDLLElasticStiff(
	double ElasticMatrix[6 * 6], ///< [in, out] the local elastic stiffness matrix as a vector
	double E, ///< Young modulus
	double mu, ///< Poisson's ratio
	double UserMaterialParams[] ///< user material parameters array
 /**
  * \retval 0 OK
  * \retval nonzero error
  */
) {
	return 0; // use the inherited elastic matrix unchanged

 /*   double const1 = REAL_MAX;
	double const2 = REAL_MAX;
	double const3 = REAL_MAX;
	int i, j;

	// Diagonal terms
	double nomin = 1.0 - mu;
	double denom = (1.0 + mu)*(1.0 - 2.0*mu);
	if( fabs(denom) > MIN_DIV*fabs(nomin) )
	{
		const1 = E * (nomin / denom);
	}
	// Off-diagonal terms
	nomin = mu;
	denom = 1.0 - mu;
	if( fabs(denom) > MIN_DIV*fabs(nomin) )
	{
	   const2 = const1 * (nomin / denom);
	}
	// Shear terms
	nomin = 1.0 - 2.0*mu;
	denom = 2.0*(1.0 - mu);
	if( fabs(denom) > MIN_DIV*fabs(nomin) )
	{
	   const3 = const1*(nomin / denom);
	}

	for(i=0; i<6; i++){
	   for(j=0; j<6; j++){
		  if(i==j) { // diagonal term
			 if(i<3){
				ElasticMatrix[i*6+j]=const1;
			 }else{ // Shear terms
				ElasticMatrix[i*6+j]=const3;
			 }
		  }else{ // nondiagonal
			 if(i<3 && j<3){
				ElasticMatrix[i*6+j]=const2;
			 }else{ // Shear terms
				ElasticMatrix[i*6+j]=0.;
			 }
		  }
	   }
	}

	return 0;
	*/
}

/**
 *
 * \brief Purpose: compute the local secant stiffness matrix.
 *
 * <b>NOT USED</b> in current version.
 *
 */
__declspec(dllexport) UINT CDECL UserDLLSecantStiff(double SecantMatrix[],
	double E, double mu, double UserMaterialParams[], double UserMaterialState[]) {
	return 0;
}

/**
 *
 * \brief Purpose: transformation of coordinate system due to large deformations.
 *
 */
__declspec(dllexport) UINT CDECL UserDLLTransformState(
	double DefGradient[], ///< the deformation gradient matrix based used for transformation of the elastic stresses and strains
	double eps[], ///< [in+out] total strain tensor (stored as a vector)
	double sigma[], ///< [in+out] stress tensor stored as a vector (as for deps)
	double E, ///< Young modulus
	double mu, ///< Poisson's ratio
	double UserMaterialParams[], ///< user material parameters array
	double UserMaterialState[] ///< [in+out] user state variables array
/**
 * \retval 0  OK, the def. gradient matrix should be used - nothing done here, and this function needs not to be called again (to reduce overhead/CPU time)
 * \retval 1  OK, the def. gradient matrix should be used - nothing done here, but this function should be called next time
 * \retval 2  OK, user's own transformation used for user material state vars, the def. matrix should be used to transform strains+stresses
 * \retval 3  OK, user's own transformation used for both user material state vars and strains+stresses
 * \retval other  error
 */
) {
	//UserMaterialState[0]=0.0;
	//UserMaterialState[1]=0.0;
	return 0;
}

/**
 *
 * \brief Purpose: The number of user defined material parameters.
 *
 * The user has to define this function to let the ATENA kernel know how many
 * additional parameters the material has, which is required among others
 * when reading the material definition with the parameter values from an input file.
 *
 * @return  the number of additional user material parameters
 *
 */
__declspec(dllexport) UINT CDECL UserDLLMaterialParamsCount() {
	return UserMaterialParamsCount; // the number of additional user material parameters
}

/**
 *
 * \brief Purpose: The number of user defined material state variables.
 *
 * The user has to define this function to let the ATENA kernel know how many
 * additional state variables the material has in each material point,
 * which is required among others when offering the list of quantities
 * available for postprocessing.
 *
 * @return  the number of additional user material state variables
 *
 */
__declspec(dllexport) UINT CDECL UserDLLStateVarsCount() {
	return UserStateVarsCount; // the number of additional user state variables
}

/**
 *
 * \brief Purpose: The name of a user defined material parameter.
 *
 * The user has to define this function to let the ATENA kernel know the
 * names of the additional parameters the material has, which is required among others
 * when reading the material definition with the parameter values from an input file.
 *
 * @param MaterialParamNo  parameter number (id) 0..UserMaterialParamsCount-1
 *
 * \retval string name of the MaterialParamNo-th user material parameter
 * \retval NULL invalid parameter number (out of range)
 *
 */
__declspec(dllexport) LPSTR CDECL UserDLLMaterialParamName(UINT MaterialParamNo) {
	if ((MaterialParamNo >= 0) && (MaterialParamNo < UserMaterialParamsCount)) {
		return UserMaterialParamNames[MaterialParamNo];
	}
	else {
		return NULL; // invalid material parameter id
	}
}

/**
 *
 * \brief Purpose: The name of a user defined material state variable.
 *
 * The user has to define this function to let the ATENA kernel know the
 * names of the additional state variables the material has,
 * which is required among others when offering the list of quantities
 * available for postprocessing.
 *
 * @param StateVarNo  parameter number (id) 0..UserStateVarsCount-1
 *
 * \retval string name of the StateVarNo-th user material state variable
 * \retval NULL invalid parameter number (out of range)
 *
 */
__declspec(dllexport) LPSTR CDECL UserDLLStateVarName(UINT StateVarNo) {
	if ((StateVarNo >= 0) && (StateVarNo < UserStateVarsCount)) {
		return UserStateVarNames[StateVarNo];
	}
	else {
		return NULL; // invalid state variable id
	}
}
/** @} */

/*main() {

	double nul[] = { 0,0,0,0,0,0 };
	double eps[] = { 0.00189474, 0, 0, 0, 0, 0,0, 0, 0 };
	double UserMaterialParams[] = { 59.e-6 ,250.e-6, 1.0 };
	double UserMaterialState[] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };


	double test = UserDLLCalculateResponse(nul, eps, nul, 34000, 0.2, UserMaterialParams, UserMaterialState);



}*/
