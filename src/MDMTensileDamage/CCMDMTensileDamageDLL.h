/**
 *  @file CCUserMaterialDLL.h
 *
 *  Author: Dobromil Pryl
 *
 *  \brief Purpose: Header file containing the definition
 *           of the user defined functions for user material.
 *
 *  Revision history:
 *
 *              1. 2008   - the file was created
 *
 */

/** @name Functions defined in the UserMaterialDLL
 *
 * @{
 */

typedef UINT (CDECL* LPUserDLLCalculateResponse)(const double deps[], const double teps[], double sigma[], 
                                                 double E, double mu, const double UserMaterialParams[], 
                                                 double UserMaterialState[]);
typedef UINT (CDECL* LPUserDLLResetState)(double E, double mu, const double UserMaterialParams[], 
                                          double UserMaterialState[]);
typedef UINT (CDECL* LPUserDLLTangentStiff)(double TangentMatrix[], 
                                            double E, double mu, 
                                            const double UserMaterialParams[], 
                                            const double UserMaterialState[]);
typedef UINT (CDECL* LPUserDLLElasticStiff)(double ElasticMatrix[], 
                                            double E, double mu, 
                                            const double UserMaterialParams[]);
typedef UINT (CDECL* LPUserDLLSecantStiff)(double SecantMatrix[], 
                                           double E, double mu, 
                                           const double UserMaterialParams[], 
                                           const double UserMaterialState[]);
typedef UINT (CDECL* LPUserDLLTransformState)(double DefGradient[], 
                                              double eps[], double sigma[], 
                                              double E, double mu, 
                                              const double UserMaterialParams[], 
                                              double UserMaterialState[]);
typedef UINT (CDECL* LPUserDLLMaterialParamsCount)();
typedef UINT (CDECL* LPUserDLLStateVarsCount)();
typedef LPSTR (CDECL* LPUserDLLMaterialParamName)(const UINT MaterialParamNo);
typedef LPSTR (CDECL* LPUserDLLStateVarName)(const UINT StateVarNo);
/** @} */

/** @name FORTRAN variants of the functions where workarounds are needed due to argument incompatibility
 *
 * @{  
 */
typedef UINT (CDECL* LPF90UserDLLCalculateResponse)(const double deps[], const double teps[], double sigma[], 
                                                 double *E, double *mu, const double UserMaterialParams[], 
                                                 double UserMaterialState[]);
typedef UINT (CDECL* LPF90UserDLLResetState)(double *E, double *mu, const double UserMaterialParams[], 
                                          double UserMaterialState[]);
typedef UINT (CDECL* LPF90UserDLLTangentStiff)(double TangentMatrix[], 
                                            double *E, double *mu, 
                                            const double UserMaterialParams[], 
                                            const double UserMaterialState[]);
typedef UINT (CDECL* LPF90UserDLLElasticStiff)(double ElasticMatrix[], 
                                            double *E, double *mu, 
                                            const double UserMaterialParams[]);
typedef UINT (CDECL* LPF90UserDLLSecantStiff)(double SecantMatrix[], 
                                           double *E, double *mu, 
                                           const double UserMaterialParams[], 
                                           const double UserMaterialState[]);
typedef UINT (CDECL* LPF90UserDLLTransformState)(double DefGradient[], 
                                              double eps[], double sigma[], 
                                              double *E, double *mu, 
                                              const double UserMaterialParams[], 
                                              double UserMaterialState[]);
//typedef UINT (CDECL* LPF90UserDLLMaterialParamsCount)();
//typedef UINT (CDECL* LPF90UserDLLStateVarsCount)();
typedef UINT (CDECL* LPF90UserDLLMaterialParamName)(LPSTR retname, UINT * strlen, const UINT * const MaterialParamNo);
typedef UINT (CDECL* LPF90UserDLLStateVarName)(LPSTR retname, UINT * strlen, const UINT * const StateVarNo);
/** @} */


double* VtoT_transf(double e_v[]);
double* get_phi_trial(double e_equiv_arr[], double e_p, double e_f);
double* get_MPNN();
double* get_e_vct_arr(double eps_eng[]);
double* get_e_N_arr(double e_vct_arr[]);
double* get_e_T_vct_arr(double e_vct_arr[]);
double* get_phi_arr(double teps[], double e_p, double e_f, double c_T);
double* get_phi_mtx(double teps[], double phi_arr[], double e_p, double e_f, double c_T);
double* get_beta_tns(double teps[], double phi_arr[], double e_p, double e_f, double c_T);
double* get_elasticity_tensor();
double* get_D4_mdm_ijmn(double beta_ijkl[]);
double* UserDLLCalculateResponse(
	double deps[], /**< strain increment tensor stored as a vector,
					*   first the diagonal terms, followed by the off-diagonal
					*/
	double teps[], ///< total strain tensor (stored as a vector)
	double sigma[], ///< [in+out] stress tensor stored as a vector (as for deps)
	double E, ///< elastic modulus
	double mu, ///< Poisson's ratio
	double UserMaterialParams[], ///< vector of user material parameter values
	double UserMaterialState[]);

//double* get_phi_arr(double teps[], double e_p, double e_f, double c_T);